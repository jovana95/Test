package net.radionica.User.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import net.radionica.User.model.User;
import net.radionica.User.service.*;
@RestController
public class UserController {
    
    @Autowired
    private UserService _userService;

    public UserService getUserService() {
        return _userService;
    }
     
    @GetMapping("get/{id}")
    public User findById(@PathVariable("id") int id)
    {
	User user=getUserService().getUserRepository().findById(id).get();
	    return user;
    }
    
    @GetMapping("delete/{id}")
    public void deleteById(@PathVariable("id") int id)
    {
	 getUserService().deleteUser(id);
    }
    
    @GetMapping("getByUsername/{name}")
    public List<User> findByUsername(@PathVariable("name") String name)
    {
	return getUserService().findByUsername(name);
    }
    
    @RequestMapping(value = "/save", method = RequestMethod.POST, 
            consumes = "application/json", produces = "application/json")
    public void saveUser(@RequestBody User user)
    {
	getUserService().save(user);
    }
}
