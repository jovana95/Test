package net.radionica.User.dao;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import net.radionica.User.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer>{

  List<User> findBy_username(String name);

}
