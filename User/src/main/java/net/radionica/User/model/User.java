package net.radionica.User.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class User implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int _id;
    @Column(unique=true)
    private String _username;
    @Column(unique=true)
    private String _password;
    private String _email;
    
    public int getId() {
        return _id;
    }
    public String getUsername() {
        return _username;
    }
    public String getPassword() {
        return _password;
    }
    
    public void setPassword(String password) {
	_password=password;
    }
    public String getEmail() {
        return _email;
    }
    
    
}
