package net.radionica.User.service;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.radionica.User.dao.UserRepository;
import net.radionica.User.model.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
@Service
public class UserService {
    @Autowired
    private UserRepository _userRepository;

    public UserRepository getUserRepository() {
        return _userRepository;
    }
    
    public Optional<User> findById(int id)
    {
	Optional<User> user=getUserRepository().findById(id);
	return user;	
    }
    
    public void save(User user)
    {
	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	String hashedPassword = passwordEncoder.encode(user.getPassword());
	user.setPassword(hashedPassword);
	getUserRepository().save(user);
    }
    
    public void deleteUser(int id)
    {
	getUserRepository().delete(findById(id).get());
    }
    
    public List<User> findByUsername(String name)
    {
	return getUserRepository().findBy_username(name);	
    }
}
